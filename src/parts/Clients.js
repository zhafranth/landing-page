import React from "react";
import Section from "elements/Section";
import Fade from "react-reveal/Fade";

import Image01 from "assets/images/clients-01.svg";
import Image02 from "assets/images/clients-02.svg";
import Image03 from "assets/images/clients-03.svg";
import Image04 from "assets/images/clients-04.svg";
import Image05 from "assets/images/clients-05.svg";

export default function Clients() {
  return (
    <Section className="clients">
      <Fade bottom delay={2500}>
        <div className="container">
          <div className="clients-inner section-inner has-top-divider has-bottom-divider">
            <ul className="list-reset">
              <li>
                <img src={Image01} alt="Clients 01" />
              </li>
              <li>
                <img src={Image02} alt="Clients 02" />
              </li>
              <li>
                <img src={Image03} alt="Clients 03" />
              </li>
              <li>
                <img src={Image04} alt="Clients 04" />
              </li>
              <li>
                <img src={Image05} alt="Clients 05" />
              </li>
            </ul>
          </div>
        </div>
      </Fade>
    </Section>
  );
}
