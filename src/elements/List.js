import React from "react";
import propTypes from "prop-types";

export default function List(props) {
  const className = ["list-reset"];
  className.push(props.className);

  if (props.isSmall) className.push("text-xs");
  return (
    <ul className={className.join(" ")}>
      {props.data.map((item, index) => {
        return (
          <li key={index} className={item.isChecked ? "is-checked" : ""}>
            {item.content}
          </li>
        );
      })}
    </ul>
  );
}

List.propTypes = {
  className: propTypes.string,
  isSmall: propTypes.bool,
  data: propTypes.array,
};
